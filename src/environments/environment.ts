// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: "AIzaSyDGWDTz05pmSU5QwkBWTbKXXmhqNsfU_Ik",
      authDomain: "zapiski-577c9.firebaseapp.com",
      databaseURL: "https://zapiski-577c9.firebaseio.com",
      projectId: "zapiski-577c9",
      storageBucket: "zapiski-577c9.appspot.com",
      messagingSenderId: "129911115435"
  }
};
