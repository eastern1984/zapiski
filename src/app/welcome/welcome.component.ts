import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import {TrainingService} from "../training/training.service";
import {Subscription} from "rxjs/Subscription";
import {DeleteNameComponent} from "../training/current-note/delete-name.component";
import {People} from "../training/people.model";
import {Exercise} from "../training/exercise.model";
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

    lineHeight = '28px';
    fontSize = '16px';
    lineHeightSubscription: Subscription;
    fontSizeSubscription: Subscription;

    names = [];
    namesSubscription: Subscription;

    constructor(private db: AngularFirestore, private dialog: MatDialog, private trainingService: TrainingService) {
        this.trainingService.clearNames();
    }

    ngOnInit() {
        this.namesSubscription = this.trainingService.namesChanged.subscribe(
            names => {
                this.names = names;
            }
        );
        this.lineHeightSubscription = this.trainingService.lineHeightChanged.subscribe(
            value => {
                this.lineHeight = value;
            }
        );
        this.fontSizeSubscription = this.trainingService.fontSizeChanged.subscribe(
            value => {
                this.fontSize = value;
            }
        );
    }

    onNameDelete(event) {
        if (this.trainingService.confirmation) {
            const dialogRef = this.dialog.open(DeleteNameComponent, {
                data: {
                    name: this.trainingService.getNames()[event.srcElement.attributes['data-index'].value]
                }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.trainingService.deleteName(event.srcElement.attributes['data-index'].value);
                } else {

                }
            });
        }
        else {
            this.trainingService.deleteName(event.srcElement.attributes['data-index'].value);
        }
    }
}
