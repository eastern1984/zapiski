import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { TrainingService } from './training.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  currentNote = false;
  exerciseSubscription: Subscription;

  constructor(private trainingService: TrainingService) {}

    openNote(id) {
        this.currentNote = true;
    }

  ngOnInit() {
      this.currentNote = false;
    this.exerciseSubscription = this.trainingService.exerciseChanged.subscribe(
      exercise => {
        if (exercise) {
          this.currentNote = true;
        } else {
          this.currentNote = false;
        }
      }
    );
  }

  onReturn() {
      console.log(123);
      this.currentNote=false;
  }
}
