import { Component, OnInit, Input} from '@angular/core';
import {TrainingService} from "../training.service";
import {Exercise} from "../exercise.model";
import { Subscription } from 'rxjs';
import {People} from "../people.model";
import {AuthService} from "../../auth/auth.service";

@Component({
    selector: 'app-update-note',
    templateUrl: './update-note.component.html',
    styleUrls: ['./update-note.component.css']
})
export class UpdateNoteComponent implements OnInit {
    @Input() save : number;
    confirmation = true;
    private lisNamesSubscription: Subscription;
    namesList: People[] =[];

    constructor(private trainingService: TrainingService, private authService: AuthService) {}

    openNote(id) {

    }

    addName(event) {
        this.trainingService.addName(event.srcElement.innerText);
    }

    ngOnInit() {
        this.lisNamesSubscription = this.trainingService.nameListChanged.subscribe(
            (list: People[]) => {
                this.namesList = list;
            }
        );
        this.trainingService.fetchInitialNameList();
    }

    sliderHeight(event) {
        this.trainingService.setLineHeight(event.value+'px');
    }

    sliderFont(event) {
        this.trainingService.setFontSize(event.value+'px');
    }

    oncheck(){
        if (this.confirmation) {
            this.confirmation = false;
        }
        else {
            this.confirmation = true;
        }
        this.trainingService.confirmation = this.confirmation;
        console.log(this.confirmation);
    }

    saveNote() {
        this.trainingService.saveCurrentNote(this.authService.getEmail());
    }

}
