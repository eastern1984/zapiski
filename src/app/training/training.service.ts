import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subject } from 'rxjs/Subject';
import { Subscription} from "rxjs";

import { Exercise } from './exercise.model';
import {People} from "./people.model";

@Injectable()
export class TrainingService {

    confirmation = true;

    namesList: People[] = [];
    private lineHeight = '28px';
    private fontSize = '16px';
    lineHeightChanged = new Subject<string>();
    fontSizeChanged = new Subject<string>();

    nameListChanged = new Subject<People[]>();

    private names = [];
    namesChanged = new Subject<string[]>();
    exerciseChanged = new Subject<Exercise>();
    exercisesChanged = new Subject<Exercise[]>();
    finishedExercisesChanged = new Subject<Exercise[]>();
    private availableExercises: Exercise[] = [];
    private runningExercise: Exercise;
    private fbSubs: Subscription[] = [];

    constructor(private db: AngularFirestore) {}

    fetchAvailableExercises() {
        this.fbSubs.push(this.db
            .collection('availableExercises')
            .snapshotChanges()
            .map(docArray => {
                return docArray.map(doc => {
                    return {
                        id: doc.payload.doc.id,
                        name: doc.payload.doc.data().name,
                        duration: doc.payload.doc.data().duration,
                        calories: doc.payload.doc.data().calories
                    };
                });
            })
            .subscribe((exercises: Exercise[]) => {
                this.availableExercises = exercises;
                this.exercisesChanged.next([...this.availableExercises]);
            }));
    }

    startExercise(selectedId: string) {
        // this.db.doc('availableExercises/' + selectedId).update({lastSelected: new Date()});
        this.runningExercise = this.availableExercises.find(
            ex => ex.id === selectedId
        );
        this.exerciseChanged.next({ ...this.runningExercise });
    }

    completeExercise() {
        this.addDataToDatabase({
            ...this.runningExercise,
            date: new Date(),
            state: 'completed'
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    cancelExercise(progress: number) {
        this.addDataToDatabase({
            ...this.runningExercise,
            duration: this.runningExercise.duration * (progress / 100),
            calories: this.runningExercise.calories * (progress / 100),
            date: new Date(),
            state: 'cancelled'
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    getRunningExercise() {
        return { ...this.runningExercise };
    }

    fetchCompletedOrCancelledExercises() {
        this.fbSubs.push(this.db
            .collection('finishedExercises')
            .valueChanges()
            .subscribe((exercises: Exercise[]) => {
                this.finishedExercisesChanged.next(exercises);
            }));
    }

    cancelSubscriptions() {
        this.fbSubs.forEach(sub => sub.unsubscribe())
    }

    private addDataToDatabase(exercise: Exercise) {
        this.db.collection('finishedExercises').add(exercise);
    }

    getNames() {
        return this.names.slice();
    }

    addName(name) {
        this.names.push(name);
        this.namesChanged.next(this.names.slice());
    }

    deleteName(index){
        if(index) {
            this.names.splice(index,1);
            this.namesChanged.next(this.names.slice());
        }
    }

    setLineHeight(value:string) {
        this.lineHeight = value;
        this.lineHeightChanged.next(this.lineHeight);
    }

    setFontSize(value:string) {
        this.fontSize = value;
        this.fontSizeChanged.next(this.fontSize);
    }

    clearNames() {
        this.names = [];
        this.namesChanged.next(this.names.slice());
    }

    fetchInitialNameList() {
        this.fbSubs.push(this.db
            .collection('names')
            .valueChanges()
            .subscribe((namesList: People[]) => {
                this.nameListChanged.next(namesList);
            }));
    }

    saveCurrentNote(email) {
        let db = this.db;
        let names = this.names;
        this.db.collection('notes').add({userEmail: email}).then(function (result) {
            for (let name of names) {
                db.collection('simpleNoteNames').add({name: name, noteId: result.id, userEmail: email});
            }
        });
        this.clearNames();
    }

    fetchNotesOfUser() {

    }
}
