import {Component, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { TrainingService } from '../training.service';
import { Exercise } from '../exercise.model';

@Component({
    selector: 'app-new-training',
    templateUrl: './new-training.component.html',
    styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit, OnDestroy {
    exercises: Exercise[];
    exerciseSubscription: Subscription;
    @Output() openNote = new EventEmitter<string>();

    constructor(private trainingService: TrainingService) {}

    ngOnInit() {
        this.exerciseSubscription = this.trainingService.exercisesChanged.subscribe(
            exercises => (this.exercises = exercises)
        );
        this.trainingService.fetchAvailableExercises();
    }

    onOpen(id) {
        this.openNote.emit(id);
        console.log(id);
    }

    onStartTraining(form: NgForm) {
        this.trainingService.startExercise(form.value.exercise);
    }

    ngOnDestroy() {
        this.exerciseSubscription.unsubscribe();
    }

    onCreate() {
        this.openNote.emit(null);
    }
}
