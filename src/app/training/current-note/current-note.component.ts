import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import {Subscription} from "rxjs/Subscription";

import {DeleteNameComponent} from './delete-name.component';
import { TrainingService } from '../training.service';

@Component({
  selector: 'app-current-note',
  templateUrl: './current-note.component.html',
  styleUrls: ['./current-note.component.css']
})
export class CurrentNoteComponent implements OnInit {

    lineHeight = '28px';
    fontSize = '16px';
    lineHeightSubscription: Subscription;
    fontSizeSubscription: Subscription;

    names = [];
    namesSubscription: Subscription;

    constructor(private dialog: MatDialog, private trainingService: TrainingService) {}

    ngOnInit() {
        this.trainingService.clearNames();
        this.namesSubscription = this.trainingService.namesChanged.subscribe(
            names => {
                this.names = names;
            }
        );
        this.lineHeightSubscription = this.trainingService.lineHeightChanged.subscribe(
            value => {
                this.lineHeight = value;
            }
        );
        this.fontSizeSubscription = this.trainingService.fontSizeChanged.subscribe(
            value => {
                this.fontSize = value;
            }
        );
    }

    onNameDelete(event) {
        if (this.trainingService.confirmation) {
            const dialogRef = this.dialog.open(DeleteNameComponent, {
                data: {
                    name: this.trainingService.getNames()[event.srcElement.attributes['data-index'].value]
                }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.trainingService.deleteName(event.srcElement.attributes['data-index'].value);
                } else {

                }
            });
        }
        else {
            this.trainingService.deleteName(event.srcElement.attributes['data-index'].value);
        }
    }
}
