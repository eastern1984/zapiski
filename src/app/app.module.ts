import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { NotesComponent } from './training/notes.component';
import { CurrentNoteComponent } from './training/current-note/current-note.component';
import { NewTrainingComponent } from './training/new-training/new-training.component';
import { SecondListComponent} from './training/second-list/second-list.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { AuthService } from './auth/auth.service';
import { TrainingService } from './training/training.service';
import { environment } from '../environments/environment';
import {UpdateNoteComponent} from "./training/update-note/update-note.component";
import {DeleteNameComponent} from "./training/current-note/delete-name.component";

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
      NotesComponent,
      CurrentNoteComponent,
    NewTrainingComponent,
      SecondListComponent,
    WelcomeComponent,
    HeaderComponent,
    SidenavListComponent,
      UpdateNoteComponent,
      DeleteNameComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [AuthService, TrainingService],
  bootstrap: [AppComponent],
  entryComponents: [DeleteNameComponent]
})
export class AppModule { }
